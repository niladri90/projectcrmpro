package src.test.java;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import src.main.java.Company;
import src.main.java.Login;
import src.utility.java.Browserfactory;
public class Companyregression {

	WebDriver driver;
	Login objlogin;
	Company objcompany;
	
	@BeforeMethod
	public void checkapplaunch() throws IOException, InterruptedException {

		driver = Browserfactory.applaunch("Chrome");
		Thread.sleep(2000);
		Browserfactory.calltestdata();
		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkvalidLogin(), true);

	}
	
	@Test(priority = 1, enabled = true, description = "Check Page load Create New Company")
	public void checkCreateCompanyPageLoad() throws InterruptedException {
		objcompany = PageFactory.initElements(driver, Company.class);
		Assert.assertEquals(objcompany.checkPageLoadCreateNewCompany(), true);
	}
	@Test(priority = 2, enabled = true, description = "Check Company name alert")
	public void checkCompanyNamealertmessage() throws InterruptedException {
		objcompany = PageFactory.initElements(driver, Company.class);
		Assert.assertEquals(objcompany.checkalertmessageforcompany(), true);
	}
	
	@Test(priority = 3, enabled = true, description = "Save Company Information")
	public void saveCompanyInformation() throws InterruptedException,IOException {
		objcompany = PageFactory.initElements(driver, Company.class);
		Assert.assertEquals(objcompany.checkCompanyInformation(), true);

		
	}
	@AfterMethod

	public void checkclosedriver() {

		Browserfactory.driverclose();
	}
		
}
