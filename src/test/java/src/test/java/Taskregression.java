package src.test.java;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import src.main.java.Contact;
import src.main.java.Login;
import src.main.java.Task;
import src.utility.java.Browserfactory;

public class Taskregression {

	WebDriver driver;
	Login objlogin;
	Task objtask;
	
	@BeforeMethod
	public void checkapplaunch() throws IOException, InterruptedException {

		driver = Browserfactory.applaunch("Chrome");
		Thread.sleep(2000);
		Browserfactory.calltestdata();
		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkvalidLogin(), true);

	}
	
	@Test(priority = 1, enabled = true, description = "Check Page load Create New Task")
	public void checkCreateTaskPageLoad() throws InterruptedException {
		objtask = PageFactory.initElements(driver, Task.class);
		Assert.assertEquals(objtask.checkPageLoadCreateNewTask(), true);
	}
	
	@Test(priority = 2, enabled = true, description = "Check alert message for the title")
	public void checkCreatealertmessagefortitle() throws InterruptedException {
		objtask = PageFactory.initElements(driver, Task.class);
		Assert.assertEquals(objtask.checkalertmessagefortitlefield(), true);
	}
	
	@AfterMethod

	public void checkclosedriver() {

		Browserfactory.driverclose();
	}
}
