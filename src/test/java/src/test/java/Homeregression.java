package src.test.java;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import src.main.java.Home;
import src.main.java.Login;
import src.utility.java.Browserfactory;

public class Homeregression {
	WebDriver driver;
	Login objlogin;
	Home objhome;

	@BeforeMethod
	public void checkapplaunch() throws IOException, InterruptedException {

		driver = Browserfactory.applaunch("Chrome");
		Thread.sleep(2000);
		Browserfactory.calltestdata();
		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkvalidLogin(), true);

	}

	@Test(priority = 1, enabled = true, description = "Check Company Tab")
	public void checkcompanytab() throws InterruptedException {
		objhome = PageFactory.initElements(driver, Home.class);
		Assert.assertEquals(objhome.checkCompaniestab(), true);
	}
	
	@Test(priority = 2, enabled = true, description = "Check New Company")
	public void checkNewCompany() throws InterruptedException {
		objhome = PageFactory.initElements(driver, Home.class);
		Assert.assertEquals(objhome.checkNewCompanyItem(), true);
	}

	@Test(priority = 3, enabled = true, description = "Check Contact Tab")
	public void checkContact() throws InterruptedException {
		objhome = PageFactory.initElements(driver, Home.class);
		Assert.assertEquals(objhome.checkContacttab(), true);
	}
	
	@Test(priority = 4,enabled = true, description = "Check New Contact")
	
	public void checkNewContact() throws InterruptedException {
		objhome = PageFactory.initElements(driver, Home.class);
		Assert.assertEquals(objhome.checkNewContactItem(), true);
	}


	@Test(priority = 5, enabled = true, description = "Check Tasks Tab")
	public void checkTasks() throws InterruptedException {
		objhome = PageFactory.initElements(driver, Home.class);
		Assert.assertEquals(objhome.checkTasksTab(), true);
	}
	
    @Test(priority = 6,enabled = true, description = "Check New Task")
	
	public void checkNewTask() throws InterruptedException {
		objhome = PageFactory.initElements(driver, Home.class);
		Assert.assertEquals(objhome.checkNewTaskItem(), true);
	}

	
	@AfterMethod

	public void checkclosedriver() {

		Browserfactory.driverclose();
	}
}
