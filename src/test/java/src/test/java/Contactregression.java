package src.test.java;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import src.main.java.Contact;
import src.main.java.Login;
import src.utility.java.Browserfactory;
public class Contactregression {

	
	WebDriver driver;
	Login objlogin;
	Contact objcontact;
	
	@BeforeMethod
	public void checkapplaunch() throws IOException, InterruptedException {

		driver = Browserfactory.applaunch("Chrome");
		Thread.sleep(2000);
		Browserfactory.calltestdata();
		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkvalidLogin(), true);

	}
	
	@Test(priority = 1, enabled = false, description = "Check Page load Create New Contact")
	public void checkCreateContactPageLoad() throws InterruptedException {
		objcontact = PageFactory.initElements(driver, Contact.class);
		Assert.assertEquals(objcontact.checkPageLoadCreateNewContact(), true);
	}
	
	@Test(priority = 2, enabled = false, description = "Check first name alert")
	public void checkfirstNamealertmessage() throws InterruptedException {
		objcontact = PageFactory.initElements(driver, Contact.class);
		Assert.assertEquals(objcontact.checkalertmessageforfirstname(), true);
	}
	
	@Test(priority = 3, enabled = true, description = "Save Contact Information")
	public void saveCompanyInformation() throws InterruptedException,IOException {
		objcontact = PageFactory.initElements(driver, Contact.class);
		Assert.assertEquals(objcontact.checkContactInformation(), true);

		
	}
	@AfterMethod

	public void checkclosedriver() {

		Browserfactory.driverclose();
	}
}
