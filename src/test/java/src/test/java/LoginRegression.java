package src.test.java;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import src.main.java.Login;
import src.utility.java.Browserfactory;



public class LoginRegression {

	WebDriver driver;
	Login objlogin;

	@BeforeMethod
	public void checkapplaunch() throws IOException {

		driver = Browserfactory.applaunch("Chrome");
		Browserfactory.calltestdata();
 
	}
	
	@Test(priority = 1,enabled= true,description="Test Username field ")

	public void checkCursorinUsername() throws InterruptedException {
		// RegistrationPage register = PageFactory.initElements(driver,
		// RegistrationPage.class);
		// register.verifyregistrationdetails();

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkcursorusernamefield(), true);

	}

	@Test(priority = 2,enabled= true,description="Enter username field")

	public void checkEnteredUsername() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkEnteredUsernameField(), true);

	}

	@Test(priority = 3,enabled= true,description="Test password field")
	public void checkCursorinPassword() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkCursorpasswordfield(), true);
	}

	@Test(priority = 4,enabled= true,description="Enter password field")

	public void checkEnteredPassword() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkEnteredPasswordfield(), true);
	}

	@Test(priority = 5,enabled= true,description="Test with blank username & password")

	public void blankFieldChecking() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkBlankField(), true);

	}

	@Test(priority = 6,enabled= true,description="Test with the invalid username & invalid password")

	public void invalidLogin() throws InterruptedException {
		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkInvalidLogin(), true);
	}

	@Test(priority = 7,enabled= true,description="Test with the valid username & valid password")

	public void validLogin() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkvalidLogin(), true);

	}
	
	@AfterMethod

	public void checkclosedriver() {

		Browserfactory.driverclose();
	}

}
