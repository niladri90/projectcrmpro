package src.utility.java;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Browserfactory {
	static WebDriver driver;

	public static WebDriver applaunch(String browsername) throws IOException {

		if (browsername.equalsIgnoreCase("firefox")) {

			driver = new FirefoxDriver();
		} else if (browsername.equalsIgnoreCase("Chrome")) {

			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();

		return driver;
	}

	public static void driverclose() {

		driver.close();
	}

	public static void calltestdata() throws IOException {

		Properties obj = new Properties();

		FileInputStream fs = new FileInputStream("./Files/testdata.properties");

		obj.load(fs);

		String homeurl = obj.getProperty("URL");

		System.out.println(homeurl);

		driver.get(homeurl);

	}

	
}
