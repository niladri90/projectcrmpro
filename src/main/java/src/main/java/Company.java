package src.main.java;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Company {

	
	WebDriver driver;

	@FindBy(linkText = "COMPANIES")
	WebElement comp;

	@FindBy(linkText = "New Company")
	WebElement newcompany;

	@FindBy(className = "fieldset")
	WebElement createnewcompany;

	@FindBy(xpath = "//input[@value='Save']")
	WebElement save;

	@FindBy(id = "company_name")
	WebElement companyname;

	@FindBy(xpath = "//input[@name='industry']")
	WebElement industryname;

	@FindBy(id = "annual_revenue")
	WebElement annualrevenue;

	@FindBy(id = "num_of_employees")
	WebElement employees;

	@FindBy(id = "phone")
	WebElement phoneno;

	@FindBy(id = "fax")
	WebElement faxno;

	@FindBy(id = "website")
	WebElement website;

	@FindBy(id = "email")
	WebElement emailid;

	@FindBy(id = "symbol")
	WebElement symbol;

	@FindBy(xpath = "//input[@name='client_lookup']")
	WebElement parentcompany;

	@FindBy(className = "datafield")
	WebElement companynamenew;

	public Company(WebDriver driver) {

		this.driver = driver;
	}

	public boolean checkPageLoadCreateNewCompany() throws InterruptedException {
		
		//check page load

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(comp).perform();

		Thread.sleep(2000);

		String text = newcompany.getAttribute("title");

		System.out.println(text);

		if ("New Company".equals(text)) {

			newcompany.click();

			Thread.sleep(2000);

			String companytext = createnewcompany.getText();

			System.out.println(companytext);

			String expectedtext = "Create New Company";

			if (companytext.contains(expectedtext)) {

				System.out.println("Test case passed successfully");
			}

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkalertmessageforcompany() throws InterruptedException {
		
		//check alert message

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(comp).perform();

		Thread.sleep(2000);

		String text = newcompany.getAttribute("title");

		System.out.println(text);

		if ("New Company".equals(text)) {

			newcompany.click();

			Thread.sleep(2000);

		}

		save.click();

		Thread.sleep(4000);

		Alert alert = driver.switchTo().alert();

		// Capturing alert message.
		String alertMessage = alert.getText();

		// Displaying alert message
		System.out.println(alertMessage);

		String expectedalertmessage = "Please enter the company's name.";

		if (alertMessage.contains(expectedalertmessage)) {

			System.out.println("Test case passed successfully");

			alert.accept();

			return true;

		}

		else {

			System.out.println("Test case failed");
			return false;
		}

	}

	public boolean checkCompanyInformation() throws IOException, InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(comp).perform();

		Thread.sleep(2000);

		String text = newcompany.getAttribute("title");

		System.out.println(text);

		if ("New Company".equals(text)) {

			newcompany.click();

			Thread.sleep(2000);

		}

		Properties obj = new Properties();

		FileInputStream fs = new FileInputStream("./Files/Companydata.properties");

		obj.load(fs);

		String strcompanyname = obj.getProperty("Companyname");

		companyname.sendKeys(strcompanyname);

		String strindustry = obj.getProperty("Industry");
		industryname.sendKeys(strindustry);

		String strannualrevenue = obj.getProperty("AnnualRevenue");

		annualrevenue.sendKeys(strannualrevenue);

		String stremployees = obj.getProperty("Employees");
		employees.sendKeys(stremployees);

		String strphone = obj.getProperty("PhoneNo");
		phoneno.sendKeys(strphone);

		String strfax = obj.getProperty("FaxNo");
		faxno.sendKeys(strfax);

		String strwebsite = obj.getProperty("Website");
		website.sendKeys(strwebsite);

		String stremail = obj.getProperty("email");
		emailid.sendKeys(stremail);

		String strsymbol = obj.getProperty("Symbol");
		symbol.sendKeys(strsymbol);

		String strparentcompany = obj.getProperty("ParentCompany");
		parentcompany.sendKeys(strparentcompany);

		save.click();

		Thread.sleep(4000);
		String strcompany = companynamenew.getText();

		System.out.println(strcompany);

		if (strcompany.contains(stremployees)) {

			System.out.println("Test case passed successfully");
			return true;

		} else {
			return false;
		}
	}
}
