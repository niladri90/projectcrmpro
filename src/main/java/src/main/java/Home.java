package src.main.java;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Home {

	WebDriver driver;

	@FindBy(linkText = "COMPANIES")
	WebElement comp;

	@FindBy(linkText = "CONTACTS")
	WebElement contact;

	@FindBy(linkText = "TASKS")
	WebElement tasks;

	@FindBy(linkText = "New Company")
	WebElement newcompany;

	@FindBy(linkText = "New Contact")
	WebElement newcontact;

	@FindBy(linkText = "New Task")
	WebElement newtask;

	@FindBy(className = "fieldset")
	WebElement createnewcompany;

	public Home(WebDriver driver) {

		this.driver = driver;
	}

	public boolean checkCompaniestab() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		String text = comp.getAttribute("title");

		System.out.println(text);

		if ("Companies".equals(text)) {

			System.out.println("Test case passed successfully");

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkNewCompanyItem() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(comp).perform();

		Thread.sleep(2000);

		String text = newcompany.getAttribute("title");

		System.out.println(text);

		if ("New Company".equals(text)) {

			System.out.println("Test case passed successfully");

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkContacttab() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(3000);

		String text = contact.getAttribute("title");

		System.out.println(text);

		if ("Contacts".equals(text)) {

			System.out.println("Test case passed successfully");

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkNewContactItem() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(contact).perform();

		Thread.sleep(2000);

		String text = newcontact.getAttribute("title");

		System.out.println(text);

		if ("New Contact".equals(text)) {

			System.out.println("Test case passed successfully");

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkTasksTab() throws InterruptedException {

		driver.switchTo().frame("mainpanel");
		Thread.sleep(3000);

		String text = tasks.getAttribute("title");

		System.out.println(text);

		if ("Tasks".equals(text)) {

			System.out.println("Test case passed successfully");

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkNewTaskItem() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(tasks).perform();

		Thread.sleep(2000);

		String text = newtask.getAttribute("title");

		System.out.println(text);

		if ("New Task".equals(text)) {

			System.out.println("Test case passed successfully");

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}
}
