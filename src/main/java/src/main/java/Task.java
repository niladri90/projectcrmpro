package src.main.java;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Task {

	WebDriver driver;

	@FindBy(linkText = "TASKS")
	WebElement tasks;

	@FindBy(linkText = "New Task")
	WebElement newtask;

	@FindBy(className = "fieldset")
	WebElement createnewtask;

	@FindBy(xpath = "//input[@value='Save']")
	WebElement save;

	public Task(WebDriver driver) {

		this.driver = driver;
	}

	public boolean checkPageLoadCreateNewTask() throws InterruptedException {

		// check page load

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(tasks).perform();

		Thread.sleep(2000);

		String text = newtask.getAttribute("title");

		System.out.println(text);

		if ("New Task".equals(text)) {

			newtask.click();

			Thread.sleep(2000);

			String strtasktext = createnewtask.getText();

			System.out.println(strtasktext);

			String expectedtext = "Task Information";

			if (strtasktext.contains(expectedtext)) {

				System.out.println("Test case passed successfully");
			}

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkalertmessagefortitlefield() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(tasks).perform();

		Thread.sleep(2000);

		String text = newtask.getAttribute("title");

		System.out.println(text);

		if ("New Task".equals(text)) {

			newtask.click();

			Thread.sleep(2000);

		}

		save.click();

		Thread.sleep(4000);

		Alert alert = driver.switchTo().alert();

		// Capturing alert message.
		String alertMessage = alert.getText();

		// Displaying alert message
		System.out.println(alertMessage);

		String expectedalertmessage = "Please enter a title for this task";

		if (alertMessage.contains(expectedalertmessage)) {

			System.out.println("Test case passed successfully");

			alert.accept();

			return true;

		}

		else {

			System.out.println("Test case failed");
			return false;
		}

	}

}
