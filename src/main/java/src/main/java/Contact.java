package src.main.java;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Contact {

	WebDriver driver;

	@FindBy(linkText = "CONTACTS")
	WebElement contact;

	@FindBy(linkText = "New Contact")
	WebElement newcontact;

	@FindBy(className = "fieldset")
	WebElement createnewcontact;

	@FindBy(xpath = "//input[@value='Save']")
	WebElement save;

	@FindBy(id = "first_name")
	WebElement firstname;

	@FindBy(id = "surname")
	WebElement lastname;

	@FindBy(xpath = "//input[@name='nickname']")
	WebElement nickname;

	@FindBy(id = "nickname")
	WebElement newnickname;

	public Contact(WebDriver driver) {

		this.driver = driver;
	}

	public boolean checkPageLoadCreateNewContact() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(contact).perform();

		Thread.sleep(2000);

		String text = newcontact.getAttribute("title");

		System.out.println(text);

		if ("New Contact".equals(text)) {

			newcontact.click();

			Thread.sleep(2000);

			String companytext = createnewcontact.getText();

			System.out.println(companytext);

			String expectedtext = "Contact Information";

			if (companytext.contains(expectedtext)) {

				System.out.println("Test case passed successfully");
			}

			return true;
		}

		else {

			System.out.println("Test case failed");

			return false;

		}

	}

	public boolean checkalertmessageforfirstname() throws InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(contact).perform();

		Thread.sleep(2000);

		String text = newcontact.getAttribute("title");

		System.out.println(text);

		if ("New Contact".equals(text)) {

			newcontact.click();

			Thread.sleep(2000);

		}

		save.click();

		Thread.sleep(4000);

		Alert alert = driver.switchTo().alert();

		// Capturing alert message.
		String alertMessage = alert.getText();

		// Displaying alert message
		System.out.println(alertMessage);

		String expectedalertmessage = "Please enter a first name";

		if (alertMessage.contains(expectedalertmessage)) {

			System.out.println("Test case passed successfully");

			alert.accept();

			return true;

		}

		else {

			System.out.println("Test case failed");
			return false;
		}

	}

	public boolean checkContactInformation() throws IOException, InterruptedException {

		driver.switchTo().frame("mainpanel");

		Thread.sleep(2000);

		Actions action = new Actions(driver);
		action.moveToElement(contact).perform();

		Thread.sleep(2000);

		String text = newcontact.getAttribute("title");

		System.out.println(text);

		if ("New Contact".equals(text)) {

			newcontact.click();

			Thread.sleep(2000);

		}

		Properties obj = new Properties();

		FileInputStream fs = new FileInputStream("./Files/ContactInformation.properties");

		obj.load(fs);

		String strfirstname = obj.getProperty("FirstName");

		firstname.sendKeys(strfirstname);
		
		String strlastname = obj.getProperty("LastName");

		lastname.sendKeys(strlastname);
		
		String strnickname = obj.getProperty("NickName");

		nickname.sendKeys(strnickname);
		
		save.click();

		Thread.sleep(4000);
		
		String strnick = newnickname.getText();

		System.out.println(strnick);

		if (strnick.equals(strnickname)) {

			System.out.println("Test case passed successfully");
			return true;

		} else {
			return false;
		}
	}
}


